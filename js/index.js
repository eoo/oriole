(function(){
	$(document).ready(function() {
	    $('.nav').on('click', function(e) {
	    	if (e.target && e.target.nodeName === 'A') {
	    	    var item = $(e.target).attr('href');
	    	    $.scrollTo(item, 500, {offset:-60});
	    	 }        
	    });
	});
})();

$(function(){
    $("ul.repair-options li").click(function(){
        $(this)
            .siblings().removeClass("active").end()
            .addClass("active");
        var n = $(this).index();
        $("ul.repair-options-content li")
        	.siblings().removeClass("visible").end();
        $("ul.repair-options-content li:eq("+n+")")
        	.addClass("visible");
    });
});


$( '.content' ).scrollspy();

jQuery(function($){
   $("#tel").mask("(999) 999-99-99");
   $("#tel").addClass("bright");
});

